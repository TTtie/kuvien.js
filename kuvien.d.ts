declare module "kuvien.io" {
    class Client {
        constructor(apiKey?: string, idToken?: string, accessToken?: string);
        upload(file: string|UploadObj, extended?: boolean): Promise<Image|object>;
        getDomains(): Promise<string[]>;
        getUserSubdomains(): Promise<Domain[]>;
        addDomain(domain: string, subdomain: string): Promise<Domain>;
        listImages(page: number): Promise<Image[]>;
        getImage(imageID: string): Promise<Image>;
    }

    export = Client;
    type UploadObj = {
        name: string;
        file: Buffer|Blob|File;
    }

    class Domain {
        subdomain: string;
        domain: string;
        apiKey: string;
        url: string;
        regenerate(): Promise<Domain>;
    }

    class Image {
        id: string;
        domain: string;
        mature: boolean;
        name: string;
        subdomain: string;
        uploadUnix: number;
        url: string;
        uploadDate: string;
    }
}