# THIS HAS BEEN DEPRECATED BECAUSE OF KUVIEN.IO'S SHUTDOWN. [READ UP AT THE KUVIEN.IO WEBSITE WHY.](https://kuvien.io)
# kuvien.js
The better kuvien.io library.  
[![NPM](https://nodei.co/npm/kuvien.io.png?downloads=true&downloadRank=true&stars=true)](https://npm.im/kuvien.io)

## Features
- No dependencies
- Supports browsers (compatible with [fetch()](https://github.github.io/fetch))
- Supports TypeScript

## Installing
Kuvien.js is available on NPM:
```
npm install kuvien.io --save
# or if you use Yarn:
yarn add kuvien.io
```

## Resources
- [GitLab](https://gitlab.com/TTtie/kuvien.js)
- [kuvien.io Discord server](https://discord.gg/n9C4fEd)
- [Examples](https://gitlab.com/TTtie/kuvien.js/tree/master/examples)