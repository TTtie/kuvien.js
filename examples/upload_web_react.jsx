import React, { Component } from "react";
import { render } from "react-dom";
import Kuvien from "../";

const client = new Kuvien("YOUR API KEY");
// Renders a container, lets an user choose an image and uploads it using the File API.
class App extends Component {
    render() {
        return (<div>
            <form>
                <input type="file" id="fileToUpload" required accept="image/*" />
                <input type="submit" onClick={() => {
                    const e = document.querySelector("input#fileToUpload");
                    client.upload({
                        file: e.files[0]
                    }).then(i => this.setState((prevState, props) => ({url: i.url})))
                }}/>
                {this.printURL};
            </form>
        </div>)
    }

    get printURL() {
        if (!this.state) return null;
        if (!this.state.url) return null;
        return (<span><a href={this.state.url}>Here is your image</a></span>);
    }
}

render(<App />, document.body);