const Kuvien = require("../");
const client = new Kuvien("YOUR API KEY");
const { readFileSync } = require("fs");

// Uploads the image using a buffer the current directory and prints the link to the console
client.upload({
    file: readFileSync("./image.png"),
    name: "image.png"
}).then(i => console.log(i.url));