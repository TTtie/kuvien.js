module.exports = (input) => {
    const buf = (input instanceof Uint8Array) ? input : new Uint8Array(input);

    if (!(buf && buf.length > 1)) {
        return null;
    }

    const check = (header, options) => {
        options = Object.assign({
            offset: 0
        }, options);

        for (let i = 0; i < header.length; i++) {
            // If a bitmask is set
            if (options.mask) {
                // If header doesn't equal `buf` with bits masked off
                if (header[i] !== (options.mask[i] & buf[i + options.offset])) {
                    return false;
                }
            } else if (header[i] !== buf[i + options.offset]) {
                return false;
            }
        }

        return true;
    };

    if (check([0xFF, 0xD8, 0xFF])) {
        return {
            ext: "jpg",
            mime: "image/jpeg"
        };
    }

    if (check([0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A])) {
        return {
            ext: "png",
            mime: "image/png"
        };
    }

    if (check([0x47, 0x49, 0x46])) {
        return {
            ext: "gif",
            mime: "image/gif"
        };
    }

    if (check([0x57, 0x45, 0x42, 0x50], {offset: 8})) {
        return {
            ext: "webp",
            mime: "image/webp"
        };
    }

    if (check([0x46, 0x4C, 0x49, 0x46])) {
        return {
            ext: "flif",
            mime: "image/flif"
        };
    }

    // Needs to be before `tif` check
    if (
        (check([0x49, 0x49, 0x2A, 0x0]) || check([0x4D, 0x4D, 0x0, 0x2A])) &&
		check([0x43, 0x52], {offset: 8})
    ) {
        return {
            ext: "cr2",
            mime: "image/x-canon-cr2"
        };
    }

    if (
        check([0x49, 0x49, 0x2A, 0x0]) ||
		check([0x4D, 0x4D, 0x0, 0x2A])
    ) {
        return {
            ext: "tif",
            mime: "image/tiff"
        };
    }

    if (check([0x42, 0x4D])) {
        return {
            ext: "bmp",
            mime: "image/bmp"
        };
    }

    if (check([0x49, 0x49, 0xBC])) {
        return {
            ext: "jxr",
            mime: "image/vnd.ms-photo"
        };
    }

    if (check([0x38, 0x42, 0x50, 0x53])) {
        return {
            ext: "psd",
            mime: "image/vnd.adobe.photoshop"
        };
    }

    return {
        mime: "application/octet-stream"
    };
};