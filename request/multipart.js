const { readFileSync } = require("fs");
const { join } = require("path");
const getType = require("../utils/getType");
function generateBoundary() {
    const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".split("");
    let bString = "";
    for (let i = 0; i <= 12; i++) bString += chars[Math.floor(Math.random() * chars.length)];
    return `-------------${bString}`;
}

class Multipart {
    constructor(rq) {
        this.rq = rq;
        this.boundary = generateBoundary();
        this.rq.setHeader("Content-Type", "multipart/form-data; boundary=" + this.boundary);
        this.buffers = [];
    }
    addFile(file, param, filename) {
        let fName;
        if (!Buffer.isBuffer(file)) {
            fName = join(process.cwd(), file).split("/").pop();
            try {
                file = readFileSync(join(process.cwd(), file));
            } catch (err) {
                throw err;
            }
        } else fName = filename;
        if (!fName) throw new Error("No filename");
        if (file.length > 5e7) throw new Error("File too large.");
        const { mime } = getType(file);
        if (mime === "application/octet-stream") throw new Error("Not an image");
        this.buffers.push(Buffer.from(`--${this.boundary}\r\n`));
        this.buffers.push(Buffer.from(`Content-Disposition: form-data; name="${param}"; filename="${fName}"\r\nContent-Type: ${mime}\r\n\r\n`));
        this.buffers.push(file);
        this.buffers.push(Buffer.from("\r\n"));
        return this;
    }

    addParam(key, val) {
        this.buffers.push(Buffer.from(`--${this.boundary}\r\nContent-Disposition: form-data; name="${key}"\r\n\r\n${val}\r\n`));
        return this;
    }
    end() {
        const b = Buffer.concat([...this.buffers, Buffer.from(`--${this.boundary}--\r\n`)]);
        this.rq.setHeader("content-length", b.length);
        return b;
    }
}
module.exports = Multipart;