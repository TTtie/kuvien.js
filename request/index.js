const isBrowser = typeof (window) !== "undefined" && typeof (navigator) !== "undefined";
let canFetch;
try {
    canFetch = "fetch" in window;
} catch(_) {
    canFetch = false;
}
const { request: nodeRequest } = !isBrowser ? require("https") : { request: () => "" };
const { URL } = !isBrowser ? require("url") : { URL: () => "" };
const mPart = require("./multipart");
const { existsSync } = !isBrowser ? require("fs") : { existsSync: () => "" };

class RequestHandler {
    constructor(client) {
        Object.defineProperty(this, "client", {
            value: client,
            writable: false,
            enumerable: false
        });
    }
    _processBody(multipart, body, rq) {
        if (isBrowser) {
            let b;
            if (typeof body == "object") {
                const form = new FormData();
                if (multipart)
                    for (let key in body) {
                        let bo = body[key];
                        if (bo.file instanceof window.File) {
                            if (!bo.file.type || !bo.file.type.startsWith("image/")) throw new Error("Not an image");
                            if (bo.file.size > 5e7) throw new Error("File too large");
                            form.append(key, bo.file, bo.file.name);
                            b = form;
                        } else if (bo.file instanceof window.Blob) {
                            if (!bo.file.type || !bo.file.type.startsWith("image/")) throw new Error("Not an image");
                            if (bo.file.size > 5e7) throw new Error("File too large");
                            if (!bo.name) throw new Error("Missing file name")
                            form.append(key, bo.file, bo.name);
                            b = form;
                        }
                    } else b = JSON.stringify(body);
            } else {
                b = typeof body == "string" ? body : JSON.stringify(body);
            }
            return b;
        } else {
            let b;
            if (typeof body == "object") {
                if (multipart)
                    for (let key in body) {
                        let bo = body[key];
                        const form = new mPart(rq);
                        if (existsSync(bo)) {
                            form.addFile(bo, key);
                            b = form.end();
                        } else if (bo.file && Buffer.isBuffer(bo.file)) {
                            form.addFile(bo.file, key, bo.name);
                            b = form.end();
                        } 
                    } else b = JSON.stringify(body);
            } else {
                b = typeof body == "string" ? body : JSON.stringify(body);
            }
            return b;
        }
    }
    getAuthHeaders(keys) {
        let o = {};
        if (keys[0]) o["x-app-key"] = this.client.apiKey;
        if (keys[1]) o["authorization"] = this.client.idToken ? `Bearer ${this.client.idToken}` : "";
        if (keys[2]) o["accesstoken"] = this.client.accessToken || "";
        return o;
    }

    request(method, url, headers, auth, body, multipart) {
        if (canFetch) {
            const opts = {
                method,
                headers: {}
            };
            Object.assign(opts.headers, headers);
            if (auth) Object.assign(opts.headers, this.getAuthHeaders(auth));
            if (body) {
                opts.body = this._processBody(multipart, body);
            }
            return window.fetch(url, opts).then(r => {
                if (r.status >= 200 && r.status <= 299) return r.json();
                else throw new Error("Request failed with status code " + r.status);
            });
        } else if (!isBrowser) {
            const { hostname, pathname, search } = new URL(url);
            let o = { method, headers };
            if (auth) Object.assign(o.headers, this.getAuthHeaders(auth));
            return new Promise((rs, rj) => {
                const req = nodeRequest(Object.assign({ hostname, path: `${pathname}${search}` }, o), (res) => {
                    const { statusCode } = res;
                    const buf = [];
                    res.on("data", b => buf.push(b));
                    res.on("end", () => {
                        if (statusCode >= 200 && statusCode <= 299) rs(JSON.parse(Buffer.concat(buf).toString()));
                        else rj(new Error("The request failed. Status code is " + statusCode+"\nBody is "+ Buffer.concat(buf).toString()));
                    });
                });
                req.on("error", (e) => {
                    rj(e);
                });
                if (body) {
                    const b = this._processBody(multipart, body, req);
                    req.end(b);
                } else req.end();
            });
        } else {
            throw new Error("Your interface doesn't neither support Fetch API/Node.js https module.\nisBrowser = " + isBrowser);
        }
    }
}

module.exports = RequestHandler;