const base = "https://api.kuvien.io";
module.exports = {
    UPLOAD: `${base}/image/upload`,
    DOMAINS: `${base}/domains`,
    USER_DOMAINS: `${base}/user/domains`,
    DOMAINS_ADD: `${base}/user/domain/add`,
    DOMAINS_DELETE: `${base}/user/domain/delete`,
    DOMAINS_REGENERATE: `${base}/user/domain/regenerate`,
    IMAGES_LIST: page => `${base}/user/images/${page}`,
    IMAGE_GET: imageID => `${base}/user/image/${imageID}`,
    IMAGE_DELETE: `${base}/user/image/delete`
};