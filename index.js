const rHandler = require("./request/index");
const constants = require("./request/constants");
const Image = require("./structs/Image");
const Domain = require("./structs/Domain");

class Client {
    /**
     * Constructs a new client.
     * @param {String} apiKey API key
     * @param {String} idToken ID token used for some functions
     * @param {String} accessToken Access token used for some functions
     */
    constructor(apiKey, idToken, accessToken) {
        Object.defineProperty(this, "apiKey", {
            value: apiKey,
            enumerable: false,
            writable: false
        });
        
        Object.defineProperty(this, "idToken", {
            value: idToken,
            enumerable: false,
            writable: false
        });
        
        Object.defineProperty(this, "accessToken", {
            value: accessToken,
            enumerable: false,
            writable: false
        });
        this.rqHandler = new rHandler(this);
    }

    /**
     * Uploads a file.
     * @param {Object|String} file The file object. If a string is passed on a node.js environment, it will get everything from the file name.
     * @param {String} [file.name] File name. Optional if a File is used for file.file
     * @param {Buffer|Blob|File} file.file The actual file. On a node.js environment this must be either a buffer or a string. On browser, this must be a File or a Blob.
     * @param {Boolean} [extended=false] Give the extended data or not
     * @return {Promise<Image|Object>} If extended, an Image is returned. If not an object with thumbnail and url properties is returned.
     */
    upload(file, extended) {
        if (!this.apiKey) throw new Error("Auth needed");
        if (!file) throw new Error("Missing the file");
        return this.rqHandler.request("POST", constants.UPLOAD, {"x-extended": ""+!!extended}, [true], {file}, true).then(i => {
            if (extended) return new Image(i.extended, this);
            return i.file;
        });
    }

    /**
     * Gets the domains
     * @return {Promise<String[]>} Available domains
     */
    getDomains() {
        return this.rqHandler.request("GET", constants.DOMAINS).then(b => b.domains);
    }

    /**
     * Gets user's subdomains. Requires an ID token and an access token.
     * @return {Promise<Domain[]>} User's subdomains
     */
    getUserSubdomains() {
        if (!this.idToken || !this.accessToken) throw new Error("Auth needed");
        return this.rqHandler.request("GET", constants.USER_DOMAINS, {}, [false, true, true]).then(r => r.domains.map(d => new Domain(d, this)));
    }

    /**
     * Adds a subdomain. Requires an ID token and an access token.
     * @param {String} domain The domain you wanna use
     * @param {String} subdomain The subdomain you wanna use
     * @return {Promise<Domain>} The new domain
     */
    addDomain(domain, subdomain) {
        if (!this.idToken || !this.accessToken) throw new Error("Auth needed");
        if (!domain || !subdomain) throw new Error("One or more parameters missing.");
        return this.rqHandler.request("POST", constants.DOMAINS_ADD, {}, [false, true, true], {
            domain,
            subdomain
        }).then(d => new Domain(d, this));
    }

    /**
     * Lists user's images. Requires an ID token and an access token.
     * @param {Number} page The page you want to see
     * @returns {Promise<Image[]>} The images
     */
    listImages(page) {
        if (!this.idToken || !this.accessToken) throw new Error("Auth needed");
        if (!page) throw new Error("Missing page");
        return this.rqHandler.request("GET", constants.IMAGES_LIST(page), {}, [false, true, true]).then(r => r.images.map(i => new Image(i, this)));
    }

    /**
     * Gets the image. Requires an ID token and an access token.
     * @param {String} imageID The image ID
     * @return {Promise<Image>}
     */
    getImage(imageID) {
        if (!this.idToken || !this.accessToken) throw new Error("Auth needed");
        if (!imageID) throw new Error("Missing image ID");
        return this.rqHandler.request("GET", constants.IMAGE_GET(imageID), {}, [false, true, true]).then(r => new Image(r.image, this));
    }
}

module.exports = Client;