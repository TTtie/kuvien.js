const constants = require("../request/constants");

class Domain {
    /**
     * Represents a domain
     * @prop {String} subdomain The subdomain
     * @prop {String} domain The domain
     * @prop {String} apiKey The API key for the domain
     */
    constructor(payload, client) {
        Object.defineProperty(this, "_client", {
            writable: false,
            enumerable: false,
            value: client
        });
        this.subdomain = payload.subdomain;
        this.domain = payload.domain;
        this.apiKey = payload.key;
    }

    /**
     * Gives the URL to the domain.
     * @return {String}
     */
    get url() {
        return `https://${this.subdomain}.${this.domain}`;
    }

    /**
     * Is meant to delete the domain, but isn't implemented yet.
     * @throws {Error}
     */
    delete() {
        throw new Error("Not implemented yet. Sorry.");
        //if (!this._client.idToken || !this._client.accessToken) throw new Error("Auth needed");
        //return this._client.rqHandler.request("DELETE", constants.DOMAINS_DELETE, {}, [false, true, true], {key: this.apiKey}).then(r => r.status)
    }

    /**
     * Regenerates the domain's API key
     * @returns {Promise<Domain>} The domain this method was called for, but with a new API key set.
     */
    regenerate() {
        if (!this._client.idToken || !this._client.accessToken) throw new Error("Auth needed");
        return this._client.rqHandler.request("PUT", constants.DOMAINS_REGENERATE, {}, [false, true, true], {key: this.apiKey}).then(r => {
            this.apiKey = r.key;
            return this;
        });
    }
}

module.exports = Domain;