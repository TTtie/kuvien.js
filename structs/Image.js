const constants = require("../request/constants");
class Image {
    /** Represents an image.
     * @prop {String} id The image ID
     * @prop {String} domain The image domain
     * @prop {Boolean} mature Determines if the image is NSFW
     * @prop {String} name The original file name
     * @prop {String} subdomain The image subdomain
     * @prop {Number} uploadUnix The unix time when the image was uploaded
     */
    constructor(payload, client) {
        Object.defineProperty(this, "_client", {
            writable: false,
            enumerable: false,
            value: client
        });
        this.id = payload._id;
        this.domain = payload.domain;
        this.mature = payload.mature;
        this.name = payload.originalname;
        this.subdomain = payload.subdomain;
        this.uploadUnix = payload.upload_ts;
    }

    /**
     * The image URL
     * @return {String}
     */
    get url() {
        return `https://${this.subdomain}.${this.domain}/${this.id}`;
    }

    /**
     * Returns the upload timestamp as a Date object.
     * @return {Date}
     */
    get uploadDate() {
        return new Date(this.uploadUnix*1000);
    }

    /**
     * Is meant to delete an image, but it's not implemented yet.
     * @throws {Error}
     */
    delete() {
        throw new Error("Not implemented yet. Sorry.");
        //if (!this._client.idToken || !this._client.accessToken) throw new Error("Auth needed");
        //return this._client.rqHandler.request("DELETE", constants.IMAGE_DELETE, {}, [false, true, true], {id: this.id}).then(r => r.status)
    }
}
module.exports = Image;